package com.demo.springboot;

import com.demo.springboot.dto.MovieListDto;

public interface MovieApiInterface {
    void MovieApiController();
    MovieListDto getMovies();
    void addMovies(String title, int year , String image);
    void updateMovies(int id , String title , int year , String image);
    void deleteMovies(int id);
}
