package com.demo.springboot;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;

import java.util.ArrayList;
import java.util.List;

public class MovieApi implements MovieApiInterface{

        List<MovieDto> moviesList = new ArrayList<>();
        private MovieListDto movies;
        private int currentId = 1;

        public MovieApi() {
            MovieApiController();
        }

    public void MovieApiController() {
        moviesList.add(new MovieDto(currentId,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        movies = new MovieListDto(moviesList);
        currentId++;
    }

    public MovieListDto getMovies() {
            moviesList.sort((a,b)-> b.getMovieId().compareTo(a.getMovieId()));
            movies = new MovieListDto(moviesList);
            return movies;
    }


    public void addMovies(String title, int year, String image) {
            moviesList.add(new MovieDto(currentId,title,year,image));
            currentId++;
    }


    public void updateMovies(int id, String title, int year, String image) {
            moviesList.set(id-1, new MovieDto(id,title,year,image));
    }

    public void deleteMovies(int id) {
        moviesList.remove(id-1);
        currentId--;
    }
}

